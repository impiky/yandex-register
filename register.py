import os
import selenium
import configparser
import pyodbc
import time
import random
import uuid
import json
import requests



from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

VMCONFIG_FILE_PATH = r'C:\\abc\\ABCMOBI.ini'
MODEM_URL = "http://192.168.8.1" # modem firmware page
YANDEX_PWD = "123456Qwe" # standart password for all yandex accounts
SIM_API_KEY = "562d16c9cc51447c86a3b034bfbb5ce4" 
NUMBER_ORDER_STATUS = "1"
ORDER_ID = "27402463"
NUMBER = "79224434976"

def getConfig(section, key, configfile):
    """
    get info form config file
    """
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
yzmuser = getConfig("vcode", "username", "config.ini")
yzmpassword = getConfig("vcode", "password", "config.ini")
yzmpath = getConfig("vcode", "yzmpath", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("execute failed")
            return False
        result = self.cur.fetchone()
        self.close()
        return result
    
    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("execute failed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results


# Captcha class
class YDMHttp:
    API_URL = 'http://api.yundama.com/api.php'
    username = ''
    password = ''
    appid = ''
    appkey = ''

    def __init__(self, username, password, appid, appkey):
        self.username = username
        self.password = password
        self.appid = str(appid)
        self.appkey = appkey

    def request(self, fields, files=[]):
        response = self.post_url(self.API_URL, fields, files)
        response = json.loads(response)
        return response

    def balance(self):
        data = {'method': 'balance', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey}
        response = self.request(data)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['balance']
        else:
            return -9001

    def login(self):
        data = {'method': 'login', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey}
        response = self.request(data)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['uid']
        else:
            return -9001

    def upload(self, filename, codetype, timeout):
        data = {'method': 'upload', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'codetype': str(codetype), 'timeout': str(timeout)}
        file = {'file': filename}
        response = self.request(data, file)
        if (response):
            if (response['ret'] and response['ret'] < 0):
                return response['ret']
            else:
                return response['cid']
        else:
            return -9001

    def result(self, cid):
        data = {'method': 'result', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'cid': str(cid)}
        response = self.request(data)
        return response and response['text'] or ''

    def decode(self, filename, codetype, timeout):
        cid = self.upload(filename, codetype, timeout)
        if (cid > 0):
            for i in range(0, timeout):
                result = self.result(cid)
                if (result != ''):
                    return cid, result
                else:
                    time.sleep(1)
            return -3003, ''
        else:
            return cid, ''

    def report(self, cid):
        data = {'method': 'report', 'username': self.username, 'password': self.password, 'appid': self.appid,
                'appkey': self.appkey, 'cid': str(cid), 'flag': '0'}
        response = self.request(data)
        if (response):
            return response['ret']
        else:
            return -9001

    def post_url(self, url, fields, files=[]):
        for key in files:
            files[key] = open(files[key], 'rb')
        res = requests.post(url, files=files, data=fields)
        return res.text


class Browser:
    """
    webdriver init Class
    """
    def __init__(self, byid, useragent):
        self.options = ChromeOptions()
        self.options.add_argument('log-level=3') # disable debug logo
        self.options.add_experimental_option('excludeSwitches', ['enable-automation'])
        self.options.add_argument('user-agent={}'.format(useragent))
        self.options.add_argument(r"user-data-dir=" + "C:\\yandexdata\\" + byid)
        # self.options.add_argument("--headless")
        # self.options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(chromepath, options=self.options)
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver, 100)
        # self.driver.get(URL)

    def close(self):
        self.driver.close()


def get_all_buyer_id(db_manager):
    """
    get all records buyer id
    """
    return db_manager.fetchall("SELECT BuyerID FROM Buyer")


def get_alie_data(db_manager, buyer_id):
    """
    get user first name, surname and browser agent from db
    """
    return db_manager.fetchone("""SELECT ENFirstName, ENLastName, BrowserUserAgent FROM Buyer WHERE BuyerID='{}'""".format(buyer_id))


def send_keys_delay_random(controller,keys,min_delay=0.05,max_delay=0.25):
    """
    send value key by key with delay
    """
    for key in keys:
        controller.send_keys(key)
        time.sleep(random.uniform(min_delay,max_delay))


# create password
def getpasssord():
    uid = str(uuid.uuid1())
    suid = ''.join(uid.split('-'))
    password4 = random.sample(suid, random.randint(6, 20))
    return ''.join(password4)


def getyzm(filename):
    # login
    username = yzmuser
    # password
    password = yzmpassword
    # app id 
    appid = 1
    # app key
    APP_KEY = '22cc5376925e9387a23cf797cb9ba745'
    # е›ѕз‰‡ж–‡д»¶
    # filename = 'C:\\Users\\lsy\\Desktop\\Pythonи°ѓз”Ёз¤єдѕ‹\\ж–°е»єж–‡д»¶е¤№\\PythonHTTPи°ѓз”Ёз¤єдѕ‹\\getimage.jpg'
    # success code http://www.yundama.com/price.html
    codetype = 1006
    # timeout in seconds
    timeout = 60
    # check for correct username
    if username == 'username':
        print('please set correct username')
    else:
        # init
        yundama = YDMHttp(username, password, appid, APP_KEY)
        # login token
        uid = yundama.login()
        print('uid: %s' % uid)
        # check balance
        balance = yundama.balance()
        print('balance: %s' % balance)
        # get image,code type and timeout
        cid, result = yundama.decode(filename, codetype, timeout)
        print('cid: %s, result: %s' % (cid, result))
        return result


def downloadimg(imagurl, imagepath, useragent, cookie):
    try:
        print("downloading image")
        header = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
            'cache-control': 'max-age=0',
            'upgrade-insecure-requests': '1',
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
        }
        cookiess = [item["name"] + "=" + item["value"] for item in cookie]
        cookiestr = '; '.join(item for item in cookiess)
        header.update({'Cookie': cookiestr})
        header.update({'user-agent': useragent})
        # imgurl = "https://c.mail.ru/c/6?0.556070617381563"
        # imagpath = "D:\\test\\yzm.png"
        r = requests.get(imagurl, headers=header)
        with open(imagepath, 'wb') as f:
            f.write(r.content)
    except Exception as e:
        print(e)


def register_yandex(db_manager, browser, first_name, last_name, useragent, pwd, phone, order_id, order_status):
    """
    register yandex mail with mobile phone confirmation
    depend on provider using different ussd codes to check mobile phone number
    """
    browser.driver.get("https://passport.yandex.by/registration/")
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div")))
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"firstname\"]"), first_name)    
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"lastname\"]"), last_name)    
    login = browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]")
    login.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/label").click()
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/div")))
    login.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/div/div/div/div/div[1]/ul/li[{}]".format(random.randint(1,5))).click() # randomly choose offering logins
    selected_login = browser.driver.find_element_by_xpath("//*[@id=\"login\"]").get_attribute("value")
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"password\"]"), pwd)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"password_confirm\"]"), pwd)
    # phone = get_phone_number(db_manager, browser) # check using phone number
    order_status, order_id, phone = buy_number(SIM_API_KEY, order_id, order_status)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"phone\"]"), phone)
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div/div"))).click()
    # sec_code = get_sms_confirmation_code(browser) # get sms code
    sec_code = recieive_sec_code(SIM_API_KEY , order_status, order_id)
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div/div[2]"))) # wait untill phone block not displayed
    browser.driver.find_element_by_xpath("//*[@id=\"phoneCode\"]").send_keys(sec_code) # type received sms code
    browser.wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/button"))).click()
    # browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/button/span"))).click()
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div/div[2]/div[2]/button").click()
    browser.wait.until(EC.invisibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div/div[2]")))
    eula = browser.driver.find_element_by_class_name("eula-popup")
    eula.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]/div/button").click()
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/button").click()
    # browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]")))
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]/div/button").click()
    browser.wait.until(EC.url_changes("https://mail.yandex.by/"))
    print("{} {}, registration complite!".format(first_name, last_name))
    browser.driver.implicitly_wait(5)
    login_to_ya_money(browser, selected_login, pwd)
    time.sleep(10000)
    return selected_login, phone, order_id, order_status

    """
    captcha implementation
    """
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div/span").click()
    # browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]")))
    # send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"hint_answer\"]"), password)
    # src = browser.driver.execute_script("return document.querySelector('.captcha__image').src;")
    # if len(src) > 0:
    #     imagepath = yzmpath + "\\" + "yzm.png"
    #     print(imagepath)
    #     if os.path.exists(imagepath):
    #         print("deleteyzm")
    #         os.remove(imagepath)
    #     print(src)
    #     downloadimg(src, imagepath, useragent, browser.driver.get_cookies())
    #     time.sleep(3)
    # if os.path.exists(imagepath):
    #     print("startyzm")
    # yzmresult = getyzm(imagepath)
    # print(yzmresult)
    # send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"captcha\"]"), str(yzmresult))
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/button").click()
    # browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]/div")))
    # eula = browser.driver.find_element_by_class_name("eula-popup")
    # eula.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]/div/button").click()
    # browser.wait.until(EC.url_changes("https://mail.yandex.by/"))
    # if "https://mail.yandex.by/" in browser.driver.current_url():
    #     print("Registration succesfull!")
    #     return True
    # browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/div")))
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/div/div/div/div/div[1]").find_elements_by_class_name("logins__list").find_elements_by_tag("li")[0].click()
    """
    captcha implementation
    """

def get_phone_number(browser):
    """
    check simcard provider and call function which send ussd query to know mobile phone number
    """
    try:
        if browser.driver.current_url().text in MODEM_URL:
            browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"all_content\"]")))
            network_provider = browser.driver.find_element_by_xpath("//*[@id=\"index_plmn_name\"]").text()
            if network_provider.text in "MTS BY":
                phone_num = ussd_query(browser, "*111*10#", network_provider.text)
            elif network_provider.text in "velcom":
                phone_num = ussd_query(browser, "*147#", network_provider.text)
            return phone_num
        else:
            pass
    except:
        browser.driver.execute_script("window.open('{}', 'new_tab')".format(MODEM_URL))
        time.sleep(2)
        browser.driver.switch_to_window(browser.driver.window_handles[2])
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"all_content\"]")))
        network_provider = browser.driver.find_element_by_xpath("//*[@id=\"index_plmn_name\"]")
        if network_provider.text in "MTS BY":
            phone_num = ussd_query(browser, "*111*10#", network_provider.text)
        elif network_provider.text in "velcom":
            phone_num = ussd_query(browser, "*147#", network_provider. text)
        return phone_num


def ussd_query(browser, ussd, provider):
    """
    sends ussd query and grab phone number 
    """
    browser.driver.find_element_by_xpath("//*[@id=\"menu_ussd\"]").click()
    try:
        if browser.driver.find_element_by_xpath("//*[@id=\"dialog\"]/div") != None:
            browser.driver.find_element_by_xpath("//*[@id=\"username\"]").send_keys("admin")
            browser.driver.find_element_by_xpath("//*[@id=\"password\"]").send_keys("admin")
            browser.driver.find_element_by_xpath("//*[@id=\"pop_login\"]").click()
        browser.wait.until(EC.invisibility_of_element_located((By.XPATH, "//*[@id=\"dialog\"]/div")))
        browser.driver.find_element_by_xpath("//*[@id=\"menu_ussd\"]").click()
        time.sleep(3)
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"all_content\"]")))
        browser.driver.find_element_by_xpath("//*[@id=\"general_command_select_input\"]").send_keys(ussd)
        browser.driver.find_element_by_xpath("//*[@id=\"general_btn\"]").click()
        ussd_result = browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"general_result_table\"]/tbody/tr[2]/td[2]/pre"))).text
        if provider in "MTS BY":
            splitted_ussd_result = str(ussd_result).split("\n")
            phone_number = str(splitted_ussd_result[0]).split(":")
            browser.close()
            browser.driver.switch_to_window(browser.driver.window_handles[1])
            return phone_number[1]
        elif provider in "velcom":
            phone_number = str(str(str(ussd_result).split("\n")[0]).split(":")[1])
            browser.close()
            browser.driver.switch_to_window(browser.driver.window_handles[1])
            return phone_number
    except:
        pass


def get_sms_confirmation_code(browser, tab_num):
    """
    get last sms data which means confirmation code for yandex
    """
    browser.driver.execute_script("window.open('{}', 'new_tab')".format(MODEM_URL))
    browser.driver.switch_to_window(browser.driver.window_handles[tab_num])
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"menu_sms\"]"))).click()
    try:
        if browser.driver.find_element_by_xpath("//*[@id=\"dialog\"]/div") != None:
            browser.driver.find_element_by_xpath("//*[@id=\"username\"]").send_keys("admin")
            browser.driver.find_element_by_xpath("//*[@id=\"password\"]").send_keys("admin")
            browser.driver.find_element_by_xpath("//*[@id=\"pop_login\"]").click()
            browser.wait.until(EC.invisibility_of_element_located((By.XPATH, "//*[@id=\"dialog\"]/div")))
        
    
    except:
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"menu_sms\"]"))).click()
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"all_content\"]")))
        time.sleep(1)
        code_message = browser.driver.find_element_by_xpath("//*[@id=\"sms_table\"]/tbody/tr[2]/td[3]/pre").text
        code = code_message.split(" ")[3]
        browser.close()
        browser.driver.switch_to_window(browser.driver.window_handles[tab_num - 1])
        return code[:-1]


def buy_number(api_key, order_id, order_status):
    """
    buy new Russian number via 5sim api 
    """
    if order_status == "0" and order_id == "0":
        balance = requests.get("http://api1.5sim.net/stubs/handler_api.php?api_key={}&action=getBalance".format(api_key)).text
        print(balance)
        balance.split(":")
        if balance == "0":
            print("NO MONEY!")
        number = requests.get("""http://api1.5sim.net/stubs/handler_api.php?api_key={}&action=getNumber&service=yandexmoney&forward=$forward&operator=$operator&country=0""".format(api_key)).text
        print(number)
        number = number.split(":")
        order_status = "1"
        return order_status, number[1], number[2]
    elif order_status == "1" and order_id != "0":
        return NUMBER_ORDER_STATUS, ORDER_ID, NUMBER



def recieive_sec_code(api_key, order_status, order_id):
    """
    get sms from api
    """
    code =""
    while True:
        time.sleep(15)
        code = requests.get("http://api1.5sim.net/stubs/handler_api.php?api_key={}&action=getStatus&id={}".format(api_key, order_id)).text
        print(code)
        time.sleep(4)
        if "STATUS_OK" in code:
            code = code.split(":")
            return code[1] 
            break


def delete_yandex_passport_number(browser):
    browser.driver.get("https://passport.yandex.by/profile/phones")
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"nb-4\"]"))).click()
    phone_accessebility_check = browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"nb-1\"]/body/div/div[1]/div[2]/div[2]/div[5]")))
    phone_accessebility_check.find_element_by_xpath("//*[@id=\"nb-1\"]/body/div/div[1]/div[2]/div[2]/div[5]/form/div/div[2]/button[1]").click()
    sms_code = get_sms_confirmation_code(browser, 1)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"yasms-code-secure\"]/span/input"), sms_code)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"yasms-password-secure\"]/span/input"), YANDEX_PWD)
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"nb-1\"]/body/div/div[1]/div[2]/div[2]/div[5]/form/div/div[1]/button"))).click

def login_to_ya_money(browser, login, pwd):
    browser.driver.execute_script("window.open('{}', 'new_window')".format("https://money.yandex.ru/"))
    browser.driver.switch_to_window(browser.driver.window_handles[1])
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div[2]/div/div[2]/span/a"))).click()
    login_block = browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div/div[2]/div/div")))
    send_keys_delay_random(login_block.find_element_by_xpath("//*[@id=\"passp-field-login\"]"), login)
    login_block.find_element_by_xpath("//*[@id=\"root\"]/div/div/div[2]/div/div/div[3]/div[2]/div/div/div[1]/form/div[3]/button[1]").click()
    browser.wait.until(EC.url_changes("https://passport.yandex.ru/auth/welcome"))
    password_block = browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div/div[2]/div/div/div[3]/div[2]")))
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div/div[2]/div/div/div[3]/div[2]/div/div/form/div[1]/div/div[1]")))
    send_keys_delay_random(password_block.find_element_by_class_name("passp-form-field__input").find_element_by_tag_name("input"), pwd)
    password_block.find_element_by_xpath("//*[@id=\"root\"]/div/div/div[2]/div/div/div[3]/div[2]/div/div/form/div[2]/button[1]").click()
    time.sleep(5)
    browser.driver.get("https://money.yandex.ru/settings")
    # browser.wait.until(EC.url_changes("https://money.yandex.ru/?from=auth"))
    # browser.driver.implicitly_wait(10)
    # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div/div[2]/div/div/a").click()
    if browser.driver.current_url == r"https://passport.yandex.ru/auth/email?origin=money&retpath=https%3A%2F%2Fmoney.yandex.ru%2F%3Ffrom%3Dauth":
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div/div[2]/div/div/div[3]/div[2]/div/form/div[3]/button"))).click()
        # browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div/div[2]/div/div/div[3]/div[2]/div/form/div[3]/button")
    # time.sleep(10)
    elif browser.driver.current_url == r"https://money.yandex.ru/reg?retpath=https%3A%2F%2Fmoney.yandex.ru%2Fsettings":
        page_content = browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div")))
        page_content.find_element_by_xpath("/html/body/div[2]/div/form/div[2]/div[2]/span[1]/button").click()
        phone = get_phone_number(browser)
        send_keys_delay_random(browser.wait.until(EC.element_to_be_clickable((By.NAME, "phone-number"))), phone)
        browser.driver.find_element_by_xpath("/html/body/div[2]/div/form/div[2]/div[3]/button").click()
        sms_code = get_sms_confirmation_code(browser, 2)
        send_keys_delay_random(browser.wait.until(EC.element_to_be_clickable((By.NAME, "sms-response"))), sms_code)
        browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div/form/div[2]/div[4]/div[2]/button"))).click()
        browser.wait.until(EC.url_changes("https://money.yandex.ru/settings"))
    # elif browser.driver.current_url == ("https://money.yandex.ru/actions" or "https://money.yandex.ru/start"):
    #     browser.driver.find_element_by_xpath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[2]/div[4]/div/a[1]/span").click()
    #     browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[7]/ul/ul/li[5]/a/div/span"))).click()
    #     browser.wait.until(EC.url_changes("https://money.yandex.ru/settings"))
    #     time.sleep(3)
        
    # elif browser.driver.current_url == "https://money.yandex.ru/?from=auth":
    #     browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[2]/div[2]/a[5]"))).click()
    #     browser.wait.until(EC.url_changes("https://money.yandex.ru/start"))
    #     browser.driver.implicitly_wait(3)
    #     browser.driver.get("https://money.yandex.ru/settings")
    # time.sleep(5)
    # print(browser.driver.current_url)
    number_change_block = browser.driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[2]/div[1]/div[3]/div[2]/div")
    number_change_block.find_element_by_xpath("/html/body/div[2]/div[2]/div[1]/div/div[2]/div[1]/div[3]/div[2]/div/div/span/button").click()
    number_change_popup = browser.wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[8]")))
    sec_code = recieive_sec_code(SIM_API_KEY, NUMBER_ORDER_STATUS, ORDER_ID)
    popup_content = number_change_popup.find_element_by_class_name("secure-auth__sms")
    popup_content.find_element_by_class_name("secure-auth__answer-input-control").send_keys(sec_code)
    browser.wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[8]/form/div[4]/button"))).click()
    browser.wait.until(EC.url_changes("https://money.yandex.ru/phoneassign"))
    if phone != None:
        send_keys_delay_random(browser.wait.until(EC.visibility_of_element_located((By.NAME, "phoneNumber"))), phone)
        browser.wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div[2]/form/div[3]/button"))).click()
        sms_code = get_sms_confirmation_code(browser, 2)
        send_keys_delay_random(browser.wait.until(EC.visibility_of_element_located((By.NAME, "answer"))), sms_code)
        browser.wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div[2]/form/div[3]/button"))).click()
    else:
        phone = get_phone_number(browser)
        send_keys_delay_random(browser.wait.until(EC.visibility_of_element_located((By.NAME, "phoneNumber"))), phone)
        browser.wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div[2]/form/div[3]/button"))).click()
        sms_code = get_sms_confirmation_code(browser, 2)
        print(sec_code)
        code_confirm_block = browser.driver.find_element_by_class_name("secure-auth__sms")
        send_keys_delay_random(code_confirm_block.find_element_by_class_name("secure-auth__answer-input-control"), sms_code)
        browser.wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[2]/div[2]/div/div[2]/form/div[3]/button"))).click()
    delete_yandex_passport_number(browser)
    # time.sleep(100000)
    


def delete_online_sim_number():
    pass


def write_to_db(db_manager, login, phone, pwd, first_name, surname):
    """
    write data to Yandex table
    """
    return db_manager.execute("""INSERT INTO Yandex (Login, MailMobile, Password,
     FirstName, Surname) VALUES('{}', '{}', '{}', '{}', '{}')""".format(login, phone, pwd, first_name, surname))


db_manager = DbManager()
buyer_id = get_all_buyer_id(db_manager)
register_count = 0
while True:
    rand_buyer = random.randint(1, len(buyer_id))
    first_name, last_name, user_agent = get_alie_data(db_manager, buyer_id[rand_buyer][0])
    try:
        if (first_name and last_name) is None:
            continue
        else:
            pass
    except: 
        pass
    # password = getpasssord()
    browser = Browser(buyer_id[rand_buyer][0], user_agent)
    yandex_login, NUMBER, ORDER_ID, NUMBER_ORDER_STATUS = register_yandex(db_manager, browser, first_name, last_name, user_agent, YANDEX_PWD, NUMBER, ORDER_ID, NUMBER_ORDER_STATUS)
    # write_to_db(db_manager, yandex_login, mobile_phone, YANDEX_PWD, first_name, last_name)
    browser.close()
    register_count += 1