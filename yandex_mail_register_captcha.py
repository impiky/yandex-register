import os
import selenium
import configparser
import pyodbc
import time
import random
import json
import requests



from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys


from python3_anticaptcha import ImageToTextTask


VMCONFIG_FILE_PATH = r'C:\\abc\\ABCMOBI.ini'
YANDEX_PWD = "123456Qwe" # standart password for all yandex accounts


def getConfig(section, key, configfile):
    """
    get info form config file
    """
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")
ANTICAPTCHA_KEY = getConfig("anticaptcha", "api_key", "anticaptcha.ini")


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False, ):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("execute failed")
            return False
        result = self.cur.fetchone()
        self.close()
        return result
    
    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("execute failed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results


class Browser:
    """
    webdriver init Class
    """
    def __init__(self, byid, useragent):
        self.options = ChromeOptions()
        self.options.add_argument('log-level=3') # disable debug logo
        self.options.add_experimental_option('excludeSwitches', ['enable-automation'])
        self.options.add_argument('user-agent={}'.format(useragent))
        self.options.add_argument(r"user-data-dir=" + "C:\\yandexdata\\" + byid) 
        # self.options.add_argument("--headless")
        # self.options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(chromepath, options=self.options)
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver, 100)
        # self.driver.get(URL)

    def close(self):
        self.driver.close()


def get_all_buyer_id(db_manager):
    """
    get all records buyer id
    """
    return db_manager.fetchall("SELECT BuyerID FROM Buyer")


def get_alie_data(db_manager, buyer_id):
    """
    get user first name, surname and browser agent from db
    """
    return db_manager.fetchone("""SELECT ENFirstName, ENLastName, BrowserUserAgent FROM Buyer WHERE BuyerID='{}'""".format(buyer_id))


def send_keys_delay_random(controller,keys,min_delay=0.05,max_delay=0.25):
    """
    send value key by key with delay
    """
    for key in keys:
        controller.send_keys(key)
        time.sleep(random.uniform(min_delay,max_delay))


def register_yandex(db_manager, browser, first_name, last_name, useragent, pwd):
    """
    register yandex mail with captcha solving
    """
    browser.driver.get("https://passport.yandex.by/registration/")
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div"))) # check form visibility
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"firstname\"]"), first_name)    
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"lastname\"]"), last_name)    
    login = browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]")
    login.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/label").click()
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/div")))
    login.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[1]/div[3]/div/div/div/div/div[1]/ul/li[{}]".format(random.randint(1,5))).click() # randomly choose offering logins
    selected_login = browser.driver.find_element_by_xpath("//*[@id=\"login\"]").get_attribute("value")
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"password\"]"), pwd)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"password_confirm\"]"), pwd)
    browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div/span").click()
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]")))
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"hint_answer\"]"), pwd)
    src = browser.driver.execute_script("return document.querySelector('.captcha__image').src;") # get link to captcha image
    print("Sending code to anticaptcha service")
    image_answer = ImageToTextTask.ImageToTextTask(anticaptcha_key = ANTICAPTCHA_KEY).captcha_handler(captcha_link=src) # get solved code from anticaptcha api
    print("Answer is: {}".format(image_answer["solution"]["text"]))
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"captcha\"]"), str(image_answer["solution"]["text"]))
    browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/button").click()
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]/div/button"))).click() # click accept eula button
    time.sleep(5)
    # try:
    #     browser.wait(EC.alert_is_present())
    #     print("Re-solving captcha")
    #     browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[1]").click()
    #     browser.driver.find_element_by_xpath("//*[@id=\"captcha\"]").clear()
    #     src = browser.driver.execute_script("return document.querySelector('.captcha__image').src;") # get link to captcha image
    #     image_answer = ImageToTextTask.ImageToTextTask(anticaptcha_key = ANTICAPTCHA_KEY).captcha_handler(captcha_link=src) # get solved code from anticaptcha api
    #     send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"captcha\"]"), str(image_answer["solution"]["text"]))
    #     browser.driver.find_element_by_xpath("//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/button").click()
    #     browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/main/div/div/div/form/div[4]/div/div[2]/div/button"))).click() # click accept eula button
    #     browser.wait.until(EC.url_changes((By.XPATH, "\profile")))
    #     time.sleep(5)
    
    # except:
        
    if browser.driver.current_url in "https://passport.yandex.by/profile":
        print("Registration succesfull!")
        print("Yandex login: {}".format(selected_login))
        print("Yandex password: {}".format(pwd))
        print("First name: {}".format(first_name))
        print("Last name: {}".format(last_name))
        print("======================================")
        return selected_login, first_name, last_name
    else:
        print("Captcha error!")
        return False

def write_yandex_login_data_to_db(db_manager, login, password, first_name, surname):
    """
    write yandex login data to DB
    """
    return db_manager.execute("""INSERT INTO Yandex (Login, Password,
     FirstName, Surname) VALUES('{}', '{}', '{}', '{}')""".format(login, password, first_name, surname))


db_manager = DbManager()
buyer_id = get_all_buyer_id(db_manager)
register_count = 0
while True:
    rand_buyer = random.randint(1, len(buyer_id))
    first_name, last_name, user_agent = get_alie_data(db_manager, buyer_id[rand_buyer][0])
    try:
        if (first_name and last_name) is None:
            continue
        else:
            pass
    except: 
        pass
    browser = Browser(buyer_id[rand_buyer][0], user_agent)
    try:
        yandex_login, first_name, surname  = register_yandex(db_manager, browser, first_name, last_name, user_agent, YANDEX_PWD)
        write_yandex_login_data_to_db(db_manager, yandex_login, YANDEX_PWD, first_name, surname)
    except:
        pass
    browser.driver.delete_all_cookies()
    browser.close()
    register_count += 1